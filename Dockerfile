FROM php:7.4-apache
label MAINTAINER="Krerk Piromsopa, PH.D. <Krerk.P@chula.ac.th>"

RUN apt-get update && apt-get install -y -f \
        wkhtmltopdf \
        libtidy-dev \
        libldap2-dev \
        unoconv \
        fonts-noto \
        zlib1g-dev \
        libzip-dev \
        libcurl4-openssl-dev \
        pkg-config  \
        libssl-dev \
        libjpeg-dev \
        libpng-dev \
        abiword \
        libonig-dev \
	curl
#RUN curl -s https://packages.microsoft.com/keys/microsoft.asc | apt-key add -
#RUN bash -c "curl -s https://packages.microsoft.com/config/debian/11/prod.list > /etc/apt/sources.list.d/mssql-release.list"
#RUN apt-get update 
#RUN ACCEPT_EULA=Y apt-get -y install msodbcsql17 mssql-tools
#RUN apt-get -y install unixodbc-dev        
RUN docker-php-ext-configure gd --with-jpeg
RUN docker-php-ext-install pdo pdo_mysql tidy ldap pcntl zip mbstring gd
#RUN pecl install mongodb sqlsrv pdo_sqlsrv
RUN pecl install mongodb
RUN docker-php-ext-enable mongodb
#RUN docker-php-ext-enable sqlsrv pdo_sqlsrv
RUN ln -s /usr/local/bin/php /usr/bin/php
RUN ln -s /usr/bin/wkhtmltopdf /usr/local/bin/wkhtmltopdf
RUN cp /etc/ssl/openssl.cnf /tmp/openssl.cnf
RUN sed 's/^CipherString = DEFAULT@SECLEVEL=2/#&/' /tmp/openssl.cnf >/etc/ssl/openssl.cnf
