# php-apache-base

Dockerfile for building base image with php:7.x-apache.
The following extensions are enabled:
- pdo
- do-mysql
- tidy
- ldap
- zip
- mongodb
- pcntl

In addition, we also include
 - wkhtmltopdf (for generating PDF)
 - unoconv (for converting odt to PDF)

 
